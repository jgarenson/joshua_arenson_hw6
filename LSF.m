function x = LSF(A,b)
%Calculates the solution to the equation Ax=b using Least Squares method.
%This method employs QR factorization.

%Find QR factorization of A
[Q,R] = qr(A);

%Solve Rx = Qb.
x = inv(R)*Q*b;