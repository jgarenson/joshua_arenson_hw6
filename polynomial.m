
%Define the two functions, exp is the expanded.
p = @(x)(x-2).^9;
p_exp = @(x)(x.^9-18*x.^8+144*x.^7-672*x.^6+2016*x.^5-4032*x.^5+5376*x.^3-4608*x.^2+2304*x-512);

%create an array for x. This is 100 equally spaced elements between 1.92
%and 2.08
x = linspace(1.92,2.08);

%Calculate the function at the x-points
f = p(x);
f_exp = p_exp(x);

%plot and name figure for reduced form.
figure
plot(x,f);
xlabel('x')
ylabel('f(x)')
title('Reduced Form of f(x)')

%plot and label figure for expanded form.
figure
plot(x,f_exp);
xlabel('x')
ylabel('f(x)')
title('Expanded Form of f(x)')