%Define our matrices
A1 = [1.0 2.0;2.0,3.9];
b1 = [1.12;2.16];

A2 = [1.000, 2.011;2.000,3.982];
b2 = [1.120;2.160];


%Calculate condition number.
C1 = norm(A1)*norm(inv(A1));
C2 = norm(A2)*norm(inv(A2));

%Print condition numbers for the user
fprintf('The condition number for A1 is: %f \n',C1)
fprintf('The condition number for A2 is: %f \n',C2)

%difference between LSF methods and MATLAB backslash cannot be seen using
%standard formatting, switching to long
% format long

%solving for x for both linear equations using Least squares factorization
x1 = LSF(A1,b1);
x2 = LSF(A2,b2);

%solving for x using backslash
x1_back = A1\b1;
x2_back = A2\b2;

%comparing values between backslash and least squares. 
diff1 = x1-x1_back
diff2 = x2-x2_back